## Install with Docker

Override paths in .env

Build and start the container

    docker-compose up --build -d
    
Apply Yii migrations
    
    docker-compose run php php yii migrate --interactive=0

You can then access the application through the following URL:

    http://127.0.0.1

