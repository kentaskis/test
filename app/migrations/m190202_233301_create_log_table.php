<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%log}}`.
 */
class m190202_233301_create_log_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%log}}', [
            'id' => $this->primaryKey()->unsigned(),
            'ts' =>$this->timestamp()->defaultExpression('CURRENT_TIMESTAMP'),
            'type' => $this->tinyInteger(2)->unsigned(),
            'message' => $this->string(250)
        ],'ENGINE = InnoDb');


        $this->createIndex(
            'log-type-idx',
            '{{%log}}',
            'type'
        );

    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {

        $this->dropIndex(
            'log-type-idx',
            '{{%log}}'
        );

        $this->dropTable('{{%log}}');
    }
}
