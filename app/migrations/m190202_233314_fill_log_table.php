<?php

use yii\db\Migration;

/**
 * Class m190202_233314_fill_log_table
 */
class m190202_233314_fill_log_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {

        $this->compact = true;
        for ($i = 1; $i < 300000; $i++) {
            $this->execute('INSERT INTO `log` (`type`, `message`) VALUES ('.rand(0,10).
                ', \'Log message number '.$i.'\');');
        }

    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->truncateTable('log');
    }

}
