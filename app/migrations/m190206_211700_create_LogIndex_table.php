<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%LogIndex}}`.
 */
class m190206_211700_create_LogIndex_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%log_index}}', [
            'id' => $this->integer()->unsigned(),
            'type' => $this->tinyInteger()->unsigned()
        ],'ENGINE=MyISAM DEFAULT CHARSET=utf8 ROW_FORMAT=FIXED');

        $this->createIndex('id_uniq','{{%log_index}}','id',true);
        $this->createIndex('log_index_type_idx','{{%log_index}}','type');

        $this->execute('INSERT INTO log_index SELECT id, type FROM log;');
    }




    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('{{%log_index}}');
    }
}
