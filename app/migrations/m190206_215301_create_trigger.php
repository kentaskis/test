<?php

use yii\db\Migration;

/**
 * Class m190206_215301_create_trigger
 */
class m190206_215301_create_trigger extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $sql = '
        DROP TRIGGER IF EXISTS `fill_log_index`;
        DELIMITER //
        CREATE TRIGGER `fill_log_index` 
        AFTER INSERT 
        ON `log` FOR EACH ROW 
        BEGIN
 	        INSERT INTO log_index ( id, type) VALUES ( NEW.id, NEW.type );
        END; //
        DELIMITER ;';
        $this->execute($sql);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->execute('DROP TRIGGER IF EXISTS `fill_log_index`;');
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m190206_215301_create_trigger cannot be reverted.\n";

        return false;
    }
    */
}
