<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "log".
 *
 * @property string $ts
 * @property int $type
 * @property string $message
 */
class Log extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'log';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['ts'], 'safe'],
            [['type'], 'integer'],
            [['message'], 'string', 'max' => 250],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'ts' => 'Ts',
            'type' => 'Type',
            'message' => 'Message',
        ];
    }

}
