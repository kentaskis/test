<?php

namespace app\models;

use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\Log;
use yii\data\ArrayDataProvider;
use yii\data\Pagination;
use yii\data\Sort;
use yii\data\SqlDataProvider;
use yii\helpers\VarDumper;

/**
 * LogQuery represents the model behind the search form of `app\models\Log`.
 */
class LogQuery extends Log
{
    const PAGE_SIZE = 100;

    private $_count;

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['type'], 'integer'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     */
    public function search($params)
    {
        $offset = ($params['page'] ?? 0) * self::PAGE_SIZE;
        $sort = new Sort([
            'attributes' => ['ts','id', 'type'],
            'params' => $params]);

        $query = LogIndex::find();
        $this->load($params);
        $query->andFilterWhere([
            'type' => $this->type
        ]);
        $this->_count = $query->count();  // ~ 	0.2 ms

        $query->limit(self::PAGE_SIZE)
            ->offset($offset)
            ->orderBy($sort->orders);
        $subSql = $query->createCommand()->getRawSql();
        $mainQuery = Log::find()->alias('l')->join('INNER JOIN',"($subSql) as i",'i.id = l.id');

        return new ArrayDataProvider([
            'allModels' => $mainQuery->asArray()->all(),
            'pagination' => false,
            'sort' => ['attributes' => ['id', 'type']]
        ]);

    }



    public function pagination()
    {
        return new Pagination([
            'totalCount' => $this->_count,
            'pageSize' => self::PAGE_SIZE,]);
    }
}
